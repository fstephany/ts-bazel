import { v1 } from 'uuid'

var greet = (name: string): string => {
    let randomUUID = v1()
    return `Hi ${name} - Here's a random UUID: ${randomUUID}`
}

export default greet