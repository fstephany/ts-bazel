# TS-Bazel

This project is a proof-of-concept for Bazel, Node, Rust, gRPC and Electron.
Bazel was chosen for its ability to manage a monorepo composed by many different
technologies while using a single build system.


## Roadmap

- [X] Simple TS library used by a TS program
- [X] Add an NPM dependency to the TS library (revisit when [issue
  443](https://github.com/bazelbuild/rules_nodejs/issues/443) is solved)
- [•] Start a devserver listening for changes and serving the app
- [•] Node or bazel first? Disimbiguate (we want bazel first)
- [•] Add an electron app 
- [•] Add a Rust server 
- [•] Having the rust server and electron app talking to each other through gRPC

## Usage

Run the program that simply displays a console message:

    # CLI App
    $ bazel run //cli:app

    # Devserver
    $ bazel run //web:devserver

## Glossary

- *ibazel*: a file watcher for Bazel

## References

- https://github.com/bazelbuild/rules_nodejs
- https://github.com/bazelbuild/rules_nodejs/tree/master/packages/typescript
- the NPM package README is pretty good: https://www.npmjs.com/package/@bazel/typescript
- https://github.com/wayou/bazel-ts-example

Tutorial from Angular people:

- https://docs.google.com/presentation/d/1OwktccLvV3VvWn3i7H2SuZkBeAQ8z-E5RdJODVLf8SA/preview#slide=id.g4e7c0f9ec5_0_136