import greet from 'demo/greeter/greeter'
console.log(greet("Bazel"))
console.log("We are in a browser with hot reloads!")

const el: HTMLDivElement = document.createElement('div')
el.innerText = 'Hello, TypeScript'
el.className = 'ts1'
document.body.appendChild(el)